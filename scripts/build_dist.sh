#!/usr/bin/env sh

# install dependencies
# apt-get update && apt-get install -y unzip curl

# download artifact
curl -L --header "Private-Token: $GIT_ACCESS_TOKEN" "$UI_JOB_ARTIFACT" -o artifact.zip
rm -rf src/main/resources/static/*
mkdir artifact
unzip artifact.zip -d artifact
unzip artifact/dist/release/*.zip -d src/main/resources/static/

# increment release
RELEASE_VERSION=$(./gradlew -q incrementVersion)
BUILD_ASSET_LOCAL_DIR="build/libs"
BUILD_ASSET_LOCAL="${BUILD_ASSET_LOCAL_DIR}/gitlab-${RELEASE_VERSION}.jar"
BUILD_ASSET_LINK="https://gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/raw/${BUILD_ASSET_LOCAL}"
ls -la $BUILD_ASSET_LOCAL_DIR

# generate build
./gradlew clean build

# reset scripts to old state
git checkout HEAD -- scripts
git config --global user.name "$GITLAB_USER_NAME"
git config --global user.email "$GITLAB_USER_EMAIL"
git remote set-url origin "https://gitlab-ci-token:$GIT_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git"
git commit -am "CI: release $RELEASE_VERSION [skip ci]"
git push origin HEAD:${CI_COMMIT_REF_NAME}

echo "export RELEASE_VERSION=$RELEASE_VERSION" >> $VARIABLES_FILE
echo "export BUILD_ASSET_LOCAL=$BUILD_ASSET_LOCAL" >> $VARIABLES_FILE
echo "export BUILD_ASSET_LINK=$BUILD_ASSET_LINK" >> $VARIABLES_FILE