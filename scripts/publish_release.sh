
#!/usr/bin/env sh

# Creata Release
release-cli create --name "Release $RELEASE_VERSION" --tag-name $RELEASE_VERSION \
  --assets-link "{\"name\":\"${RELEASE_VERSION}\",\"url\":\"${BUILD_ASSET_LINK}\"}"